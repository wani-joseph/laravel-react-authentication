<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register', 'Auth\AuthController@register')->name('register');
Route::post('/login', 'Auth\AuthController@login')->name('login');
Route::middleware('auth:api')->get('/logout', 'Auth\AuthController@logout')->name('logout');
Route::middleware('auth:api')->get('/auth-user', 'Auth\AuthController@authUser')->name('auth-user');
