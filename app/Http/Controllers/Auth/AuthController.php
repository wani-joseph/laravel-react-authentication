<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    public function register(Request $request) {
        $rules = [
            'name' => 'required',
            'email'     => 'required|email|unique:users',
            'password'  => 'required|min:6|confirmed',
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $data['password'] = bcrypt($request['password']);

        $user = User::create($data);

        $accessToken = $user->createToken('authToken')->accessToken;

        return response()->json(['access_token' => $accessToken, 'user' => $user], 200);
    }

    public function login(Request $request)
    {
        $rules = [
            'email'     => 'required|email',
            'password'  => 'required',
        ];

        $this->validate($request, $rules);

        $credentials = $request->all();

        if (!auth()->attempt($credentials)) {
            return response(['message' => 'Invalid credentials']);
        }

        $accessToken = auth()->user()->createToken('authToken')->accessToken;

        return response()->json(['access_token' => $accessToken, 'user' => auth()->user()], 200);
    }

    public function logout (Request $request) {

        $token = $request->user()->token();
        $token->revoke();

        return response()->json(['message' => 'You have been succesfully logged out!', 200]);
    }

    public function authUser()
    {
        $user = auth()->user();

        return response()->json(['user' => $user]);
    }
}
