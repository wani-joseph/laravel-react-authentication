import React from 'react'
import Button from "@material-ui/core/Button"
import TextField from "@material-ui/core/TextField"

const LoginForm = (props) => {
    const {
        values: { email, password },
        errors,
        touched,
        handleChange,
        isValid,
        setFieldTouched,
        handleSubmit
    } = props

    const change = (name, e) => {
        e.persist();
        handleChange(e);
        setFieldTouched(name, true, false)
    };

    return (
        <form onSubmit={handleSubmit}>
            <TextField
                id="email"
                name="email"
                helperText={touched.email ? errors.email : ""}
                error={touched.email && Boolean(errors.email)}
                label="Email"
                fullWidth
                value={email}
                onChange={change.bind(null, "email")}
            />
            <TextField
                id="password"
                name="password"
                helperText={touched.password ? errors.password : ""}
                error={touched.password && Boolean(errors.password)}
                label="Password"
                fullWidth
                type="password"
                value={password}
                onChange={change.bind(null, "password")}
            />
            <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                disabled={!isValid}
            >
                Login
            </Button>
        </form>
    );
};

export default LoginForm