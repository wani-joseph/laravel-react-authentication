import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme => ({
    siteLogo: {
        display: 'flex',
        alignItems: 'center'
    },
    menuIcon: {
        marginRight: theme.spacing(1),
        padding: 0,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        }
    },
    logo: {
        fontFamily: 'Montserrat',
        fontSize: 20,
        fontWeight: 700,
        textDecoration: 'none',
        letterSpacing: -1,
        color: 'white',
        [theme.breakpoints.up('md')]: {
            fontSize: 30,
            letterSpacing: -2,
        }
    },
}))