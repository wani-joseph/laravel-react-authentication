import React from 'react'
import { Link } from 'react-router-dom'
import SideDrawer from '../SideDrawer/SideDrawer'
import { useStyles } from './LogoStyles'

const Logo = props => {
    const classes = useStyles()

    return (
        <React.Fragment>
            {props.sideDrawer
                ?<div className={classes.siteLogo}>
                    <SideDrawer />
                    <Link to="/" className={classes.logo}>LaravelReactAuth</Link>
                </div>
                : <Link to="/" className={classes.logo}>LaravelReactAuth</Link>}
        </React.Fragment>
    );
};

export default Logo;