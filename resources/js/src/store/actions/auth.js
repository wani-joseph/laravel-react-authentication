import * as actionTypes from './actionTypes';
import axios from 'axios'

export const authStart = () => ({
    type: actionTypes.AUTH_START
});

export const authSuccess = (token) => ({
    type: actionTypes.AUTH_SUCCESS,
    token,
});

export const authUser = (user) => ({
    type: actionTypes.AUTH_USER,
    user,
});

export const authFail = errors => ({
    type: actionTypes.AUTH_FAIL,
    errors
});

export const authLogout = () => ({
    type: actionTypes.AUTH_LOGOUT
})

export const register = data => {
    return dispatch => {
        dispatch(authStart())

        axios.post('/api/register', data)
            .then(res => {
                localStorage.setItem('token', res.data.access_token);
                dispatch(authSuccess(res.data.access_token));
            })
            .catch(err => dispatch(authFail(err)))
    }
}

export const login = credentials => {
    return dispatch => {
        dispatch(authStart())

        axios.post('/api/login', credentials)
            .then(res => {
                localStorage.setItem('token', res.data.access_token);
                dispatch(authSuccess(res.data.access_token));
            })
            .catch(err => dispatch(authFail(err)))
    }
}

export const logout = () => {
    return dispatch => {
        axios({
            method: 'get',
            url: '/api/logout',
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(() => {
            localStorage.removeItem('token')
            dispatch(authLogout())
        })
        .catch(err => dispatch(authFail(err)))
    }
}

export const authCheckState = () => {
    return dispatch => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(logout(token));
        } else {
            dispatch(authSuccess(token)); 
        }
    }
}