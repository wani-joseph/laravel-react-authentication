import * as actionTypes from '../actions/actionTypes'
import { updateObject } from '../../shared/utility'

const initialState = {
    loading: false,
    errors: null,
    token: null,
    authUser: null,
    authRedirectPath: '/dashboard'
}

const authStart = (state, action) => {
    return updateObject(state, { loading: true });
}

const authSuccess = (state, action) => {
    return updateObject(state, {
        loading: false,
        errors: null,
        token: action.token,
    });
}

const authUser = (state, action) => {
    return updateObject(state, { profile: action.user });
}

const authFail = (state, action) => {
    return updateObject(state, {
        loading: false,
        errors: action.errors,
        token: null,
    });
}

const authLogout = (state, action) => {
    return updateObject(state, {
        loading: false,
        errors: null,
        token: null,
        authUser: null,
    });
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.AUTH_START: return authStart(state, action);
        case actionTypes.AUTH_SUCCESS: return authSuccess(state, action);
        case actionTypes.AUTH_USER: return authUser(state, action);
        case actionTypes.AUTH_FAIL: return authFail(state, action);
        case actionTypes.AUTH_LOGOUT: return authLogout(state, action);
        default: return state;
    }
}

export default reducer;