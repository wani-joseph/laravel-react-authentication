import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import { 
    createStore, 
    applyMiddleware, 
    compose,
    combineReducers
} from 'redux'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import { 
    createMuiTheme, 
    responsiveFontSizes
} from '@material-ui/core/styles'
import { ThemeProvider } from '@material-ui/styles'
import CssBaseline from '@material-ui/core/CssBaseline'

import App from './App'
import authReducer from './store/reducers/auth'

const composeEnhancers =
  typeof window === 'object' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?   
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
    }) : compose;

const reducer = combineReducers({
    auth: authReducer,
});

const enhancer = composeEnhancers(
    applyMiddleware(thunk)
);

const store = createStore(reducer, enhancer);

let theme = createMuiTheme({
    palette: {
        primary: {
            light: '#d32f2f',
            main: '#B32024',
            dark: '#801313',
            contrastText: '#fff',
        },
        secondary: {
            light: '#009688',
            main: '#008489',
            dark: '#004d40',
            contrastText: '#fff',
        },
        background: {
            default: '#F9F9F9',
        }
    },
})

theme = responsiveFontSizes(theme)

const app = (
    <Provider store={store}>
        <BrowserRouter>
            <ThemeProvider theme={theme}>
                <React.Fragment>
                    <CssBaseline />
                    <App />
                </React.Fragment>
            </ThemeProvider>
        </BrowserRouter>
    </Provider>
)

if (document.getElementById('app')) {
    ReactDOM.render(app, document.getElementById('app'))
}