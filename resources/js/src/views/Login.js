import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { Formik } from "formik"
import withStyles from "@material-ui/core/styles/withStyles"
import LoginForm from '../components/LoginForm/LoginForm'
import Logo from '../components/Logo/Logo'
import {
    Container,
    Paper
} from "@material-ui/core"
import * as Yup from "yup"
import * as actions from '../store/actions'

const styles = theme => ({
    logoContainer: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: theme.spacing(6),
    },
    logo: {
        background: theme.palette.primary.dark,
        textAlign: 'center',
        borderRadius: 4,
        padding: theme.spacing(1, 2),
    },
    paper: {
        marginTop: theme.spacing(4),
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        padding: theme.spacing(2)
    },
});

const validationSchema = Yup.object({
    email: Yup.string("Enter your email").email("Enter a valid email").required("Email is required"),
    password: Yup.string("").min(8, "Password must contain at least 8 characters").required("Enter your password")
});

class Login extends Component {
    constructor() {
        super()

        this.handleLogin = this.hundleSubmit.bind(this)
    }

    hundleSubmit(values, actions) {
        const { onLogin } = this.props

        const data = {
            'email': values.email,
            'password': values.password,
        }
        onLogin(data)
        actions.setSubmitting(false);
    }

    render() {
        const { loading, isAuthenticated, authPath, classes} = this.props;

        if (isAuthenticated)
            return <Redirect to={authPath} />

        const values = { email: "", password: "" };

        return (
            <Container maxWidth="sm">
                <div className={classes.logoContainer}>
                    <span className={classes.logo}><Logo /></span>
                </div>
                <Paper className={classes.paper} elevation={0}>
                    <Formik
                        render={props => <LoginForm {...props} />}
                        initialValues={values}
                        validationSchema={validationSchema}
                        onSubmit={this.handleLogin}
                    />
                </Paper>
            </Container>
        );
    }
}

const mapStateToProps = state => ({
    loading: state.auth.loading,
    errors: state.auth.errors,
    isAuthenticated: state.auth.token != null,
    profile: state.auth.profile,
    authPath: state.auth.authRedirectPath
})

const mapDispatchToProps = dispatch => ({
    onLogin: (data) => dispatch(actions.login(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Login))