import React from 'react'
import { Link } from 'react-router-dom'
import {
    AppBar,
    Toolbar,
    Button,
    Typography
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import Logo from '../components/Logo/Logo'

const useStyles = makeStyles(theme => ({
    toolbar: {
        display: 'flex',
        justifyContent: 'space-between'
    },
    pageContent: {
        display: 'flex',
        width: '100vw',
        height: '100vh',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'relative',
    },
    welcomeText: {
        maxWidth: '80%',
        margin: '0 auto',
        lineHeight: '42px'
    }
}))

const Welcome = () => {
    const classes = useStyles()

    return (
        <React.Fragment>
            <AppBar position="fixed">
                <Toolbar className={classes.toolbar}>
                    <Logo/>
                    <Button
                        variant="contained"
                        color="primary"
                        component={Link}
                        to="/register"
                    >
                        Account
                    </Button>
                </Toolbar>
            </AppBar>
            <main className={classes.pageContent}>
                <Typography
                    className={classes.welcomeText}
                    variant="h3"
                    component="h1"
                    color="textPrimary"
                    align="center"
                >
                    Welcome to Laravel React Authentication
                </Typography>
            </main>
        </React.Fragment>
    );
};

export default Welcome