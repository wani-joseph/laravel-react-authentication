import React from 'react'
import { connect } from 'react-redux'
import { Button } from '@material-ui/core'
import * as actions from '../store/actions'

const Dashboard = props => {

    const handleLogout = () => {
        props.onLogout();
    }

    return (
        <Button onClick={handleLogout}>Logout</Button>
    );
}

const mapDispatchToProps = dispatch => ({
    onLogout: () => dispatch(actions.logout())
})

export default connect(null, mapDispatchToProps)(Dashboard);