import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { Formik } from "formik"
import withStyles from "@material-ui/core/styles/withStyles"
import RegisterForm from '../components/RegisterForm/RegisterFom'
import Logo from '../components/Logo/Logo'
import {
    Container,
    Paper
} from "@material-ui/core"
import * as Yup from "yup"
import * as actions from '../store/actions'

const styles = theme => ({
    logoContainer: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: theme.spacing(6),
    },
    logo: {
        background: theme.palette.primary.dark,
        textAlign: 'center',
        borderRadius: 4,
        padding: theme.spacing(1, 2),
    },
    paper: {
        marginTop: theme.spacing(4),
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        padding: theme.spacing(2)
    },
});

const validationSchema = Yup.object({
    name: Yup.string("Enter a name").required("Name is required"),
    email: Yup.string("Enter your email").email("Enter a valid email").required("Email is required"),
    password: Yup.string("").min(8, "Password must contain at least 8 characters").required("Enter your password"),
    confirmPassword: Yup.string("Enter your password").required("Confirm your password").oneOf([Yup.ref("password")], "Password does not match")
});

class Register extends Component {
    constructor() {
        super()

        this.handleRegister = this.hundleSubmit.bind(this)
    }

    hundleSubmit(values, actions) {
        const { onRegister } = this.props

        const data = {
            'name': values.name,
            'email': values.email,
            'password': values.password,
            'password_confirmation': values.confirmPassword
        }
        onRegister(data)
        actions.setSubmitting(false);
    }

    render() {
        const { loading, isAuthenticated, authPath, classes} = this.props;

        if (isAuthenticated)
            return <Redirect to={authPath} />

        const values = { name: "", email: "", confirmPassword: "", password: "" };

        return (
            <Container maxWidth="sm">
                <div className={classes.logoContainer}>
                    <span className={classes.logo}><Logo /></span>
                </div>
                <Paper className={classes.paper} elevation={0}>
                    <Formik
                        render={props => <RegisterForm {...props} />}
                        initialValues={values}
                        validationSchema={validationSchema}
                        onSubmit={this.handleRegister}
                    />
                </Paper>
            </Container>
        );
    }
}

const mapStateToProps = state => ({
    loading: state.auth.loading,
    errors: state.auth.errors,
    isAuthenticated: state.auth.token != null,
    profile: state.auth.profile,
    authPath: state.auth.authRedirectPath
})

const mapDispatchToProps = dispatch => ({
    onRegister: (data) => dispatch(actions.register(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Register))