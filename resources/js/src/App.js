import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
    Switch,
    Route
} from 'react-router-dom'
import Home from './pages/Home'
import Login from './pages/Login'
import Register from './pages/Register'
import Dashboard from './pages/Dashboard'
import * as actions from './store/actions'

class App extends Component {
    componentDidMount() {
        this.props.onTryAutoSignup()
    }

    render() {
        return (
            <Switch>
                <Route path="/dashboard" component={Dashboard} />
                <Route path="/register" component={Register} />
                <Route path="/login" component={Login} />
                <Route exact path="/" component={Home} />
            </Switch>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    onTryAutoSignup: () => dispatch(actions.authCheckState())
})

export default connect(null, mapDispatchToProps)(App);