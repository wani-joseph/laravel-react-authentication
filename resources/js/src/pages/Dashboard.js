import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import DashboardView from '../views/Dashboard'

class Dashboard extends Component {
    render() {
        const { isAuthenticated } = this.props

        if (!isAuthenticated) return <Redirect to="/login" />

        return <DashboardView />
    }
}

const mapStateToProps = state => ({
    isAuthenticated: state.auth.token != null,
})

export default connect(mapStateToProps)(Dashboard)