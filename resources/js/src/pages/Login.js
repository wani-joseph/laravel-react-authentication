import React from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import LoginView from '../views/Login'

const Login = props => {
    const { isAuthenticated, authPath} = props;

    if (isAuthenticated) {
        return <Redirect to={authPath} />
    }

    return <LoginView />
};

const mapStateToProps = state => ({
    isAuthenticated: state.auth.token != null,
    authPath: state.auth.authRedirectPath
})

export default connect(mapStateToProps)(Login)